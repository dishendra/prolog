customer(tom,smith,100.0).
customer(sally,smith,230.0).

get_cust_bal(FName, LName) :-
	customer(FName,LName, Bal),
	write(FName), tab(1),
	format('~w owes us $~2f ~n',[LName,Bal]).

vertical(line(point(X,Y),point(X,Y2))).
horizontal(line(point(X,Y),point(X2,Y))).

/*

 'alice' = alice.		equality 
 \+ alice = bob.		not_equal 
 3 > 2.
 trace.  				debugging
 notrace				off debugging

*/

/*
 Equality of Expression

 5+4 =:= 4+5.

 5+4 =\= 5+4.

*/


/*
 Recursion

*/

/*
 Operators

 ;  (OR)
 , (AND)

 mod()
 random(1,10,X).
 between(1,10,X).  analogous to range() in python
 succ(2,X). 	   similar to  X=2; X++;
 abs()
 max()
 round()
 truncate()
 floor()
 ceiling()
*/

/*
 Output
 
 write('Hello World').
 writeq('Hello World').

 Input

 say_hi :-
 write('Your Name? '),
 read(X),
 format('Hi ~w !',[X]).

*/

/*
 File IO
*/

 write_to_file(File, Text) :-
  open(File, write, Stream),
  write(Stream, Text), nl,
  close(Stream).

read_file(File) :-
 open(File, read, Stream),
 get_char(Stream, Char1),
 process_stream(Char1, Stream),
 close(Stream).

process_stream(end_of_file, _) :- !.

process_stream(Char, Stream) :-
 write(Char),
 get_char(Stream,Char2),
 process_stream(Char2, Stream).

 /*
 
 assert()   append new clause
 asserta()  append new clasue in beginting
 retract()  delte clasue
 retractall()

 */


 /*
  list

  write([a|[b,c]]).
  length([1,2,3],X).
  
  [H|T] = [a,b,c].
  [_,_,[X|Y],_,Z|T] = [a,b,[c,d,e],f,g,h].

  List = [a,b,c].
  member(X,[a,b,c,d]).
  reverse([a,b,c,d]).
  append([a,b,c,d],[1,2,3],X).

  name('A random string',X).
  atom_length('string',X).		length of string
  name('deepu',List),nth0(0,List,FChar),put(FChar).
*/